class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name.capitalize
    @last_name = last_name.capitalize
    @courses = []
  end

  def name
    "#{first_name} #{last_name}"
  end

  def enroll(new_course)
    courses.each do |course|
      raise "schedule conflict" if course.conflicts_with?(new_course)
    end

    if !courses.include?(new_course)
      courses << new_course
    end
    new_course.students << self
  end

def course_load
  course_load = Hash.new(0)
  self.courses.each do |course|
    course_load[course.department] += course.credits

  end
  return course_load
end

end

# / @student = Student.new
